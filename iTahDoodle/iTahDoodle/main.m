//
//  main.m
//  iTahDoodle
//
//  Created by Andrew Burger on 4/24/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BNRAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BNRAppDelegate class]));
    }
}
